<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Report;
use App\Models\Response;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user_1 = User::create([
            'username' => 'admin123',
            'password' => Hash::make('admin123'),
            'level' => 'admin'
        ]);
        $user_2 = User::create([
            'username' => 'staff123',
            'password' => Hash::make('staff123'),
            'level' => 'staff'
        ]);
        $user_3 = User::create([
            'username' => 'agus123',
            'password' => Hash::make('agus123'),
            'level' => 'student'
        ]);
        $user_4 = User::create([
            'username' => 'christian123',
            'password' => Hash::make('christian123'),
            'level' => 'student'
        ]);
        $user_5 = User::create([
            'username' => 'christopher123',
            'password' => Hash::make('christopher123'),
            'level' => 'student'
        ]);
        $user_6 = User::create([
            'username' => 'levin123',
            'password' => Hash::make('levin123'),
            'level' => 'student'
        ]);
        $user_7 = User::create([
            'username' => 'michael123',
            'password' => Hash::make('michael123'),
            'level' => 'student'
        ]);
        $user_8 = User::create([
            'username' => 'reymond123',
            'password' => Hash::make('reymon123'),
            'level' => 'student'
        ]);
        $user_9 = User::create([
            'username' => 'bryan123',
            'password' => Hash::make('bryan123'),
            'level' => 'student'
        ]);

        $student_3 = Student::create([
            'name' => 'Agustinus',
            'grade' => 'XII',
            'phone' => '082823789874',
            'address' => 'Cimahi',
            'user_id' => $user_3->id
        ]);
        $student_4 = Student::create([
            'name' => 'Christian',
            'grade' => 'XII',
            'phone' => '08298382178',
            'address' => 'Kopo',
            'user_id' => $user_4->id
        ]);
        $student_5 = Student::create([
            'name' => 'Christopher',
            'grade' => 'XII',
            'phone' => '0829830982',
            'address' => 'Caringin',
            'user_id' => $user_5->id
        ]);
        $student_6 = Student::create([
            'name' => 'Levin',
            'grade' => 'XII',
            'phone' => '0829380190',
            'address' => 'Bandung',
            'user_id' => $user_6->id
        ]);
        $student_7 = Student::create([
            'name' => 'Michael',
            'grade' => 'XII',
            'phone' => '08292918745',
            'address' => 'Pasir Koja',
            'user_id' => $user_7->id
        ]);
        $student_8 = Student::create([
            'name' => 'Reymond',
            'grade' => 'XII',
            'phone' => '0892810902',
            'address' => 'Cibadak',
            'user_id' => $user_8->id
        ]);
        $student_9 = Student::create([
            'name' => 'Bryan',
            'grade' => 'XII',
            'phone' => '0892810902',
            'address' => 'Cimahi',
            'user_id' => $user_9->id
        ]);
    }
}
