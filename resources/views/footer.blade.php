<footer class="text-lg-start mt-4">
    <section class="">
      <div class="container text-center text-md-start d-print-none">
        <div class="row">
          <div class="col-md-3 col-lg-4 col-xl-3 mx-auto">
            <h6 class="footer-title text-uppercase fw-bold mt-4">GPS</h6>
            <h6 class="footer-title-2">Gracia Peduli Sesama</h6>
            <p>&bull; Pelaporan tindak bullying</p>
            <p>&bull; Edukasi Pencegahan bullying</p>
            <p>&bull; Tindak lanjut kasus bullying</p>
          </div>
          <div class="col-md-4 col-lg-2 col-xl-2 mx-auto mb-4">
            <h6 class="sub-title-footer-2 text-uppercase fw-bold">Pages</h6>
            <li><a href="/student/reports/create" class="text-reset">Home</a></li>
            <li><a href="/student/reports/create" class="text-reset">About</a></li>
          </div>
          <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
            <h6 class="sub-title-footer-2 text-uppercase fw-bold">Social Media</h6>
            <p><i class="bi bi-telephone-fill"></i> 012345678910</p>
            <p><i class="bi bi-whatsapp"></i> 12345678910</p>
            <p><i class="bi bi-instagram"></i> @example_232</p>
          </div>
        </div>
      </div>
    </section>
</footer>