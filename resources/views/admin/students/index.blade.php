@extends('layouts.main')

@section('content')
    <div class="main-content">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h1>Data Students</h1>

                        <p>{{ $student_list->links() }}</p>

                        <table class="table border">
                            <thead>
                                <tr class="border">
                                    <th>No.</th>
                                    <th>Name</th>
                                    <th>Grade</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Username</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($student_list as $student)
                                    <tr class="border-bottom">
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $student->name }}</td>
                                        <td>{{ $student->grade }}</td>
                                        <td>{{ $student->phone }}</td>
                                        <td>{{ $student->address }}</td>
                                        <td>{{ $student->user->username }}</td>
                                        <td>
                                            <a href="/dashboard/students/{{ $student->id }}" class="btn btn-primary"><i class="fa-solid fa-pen-to-square" style="font-size: 15px"></i></a>
                                            <a href="#" class="btn btn-danger" data-toggle="modal"
                                                data-target="#modal-{{ $student->id }}"><i class="fa-solid fa-trash" style="font-size: 15px"></i></a>
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="modal-{{ $student->id }}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Confirm</h5>
                                                    <button type="button" class="btn-close"
                                                        data-bs-dismiss="modal"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Student ID {{ $student->id }} will be deleted</p>
                                                    <p>Continue?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <form action="/dashboard/students/{{ $student->id }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                        <button type="submit" class="btn btn-secondary"
                                                            data-bs-dismiss>Cancel</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </tbody>
                        </table>
                        <a href="/dashboard/students/create" class="btn btn-success"><i class="fa-solid fa-plus"></i></a>
                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <p class="text-danger">{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('sweetalert::alert')
@endsection
