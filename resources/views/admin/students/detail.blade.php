@extends('layouts.main')

@section('content')
    <div class="main-content">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h1>Detail User</h1>
                        <form action="/dashboard/students/{{ $student->id }}" method="POST">
                            @csrf
                            @method('PATCH')
                            <div class="row flex-column">
                                <div class="col-3 mb-3">
                                    <label for="Name" class="form-label">Name</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                        value="{{ $student->name }}">
                                </div>
                                <div class="col-3 mb-2">
                                    <label for="Grade" class="form-label">Grade</label>
                                    <input type="text" class="form-control" id="grade" name="grade"
                                        value="{{ $student->grade }}">
                                </div>
                                <div class="col-3 mb-2">
                                    <label for="Phone" class="form-label">Phone</label>
                                    <input type="text" class="form-control" id="phone" name="phone"
                                        value="{{ $student->phone }}">
                                </div>
                                <div class="col-3 mb-2">
                                    <label for="Address" class="form-label">Address</label>
                                    <input type="text" class="form-control" id="address" name="address"
                                        value="{{ $student->address }}">
                                </div>
                                <div class="col-3 mb-3">
                                    <label for="username" class="form-label">Username</label>
                                    <input type="text" class="form-control" id="username" name="username"
                                        value="{{ $student->user->username }}" disabled>
                                </div>
                                <div class="col-3 mb-3">
                                    <label class="form-label">Level</label>
                                    <select name="level" id="form-select" disabled>
                                        @foreach (['admin', 'staff', 'student'] as $item)
                                            <option value="{{ $item }}">{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </form>
                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <p class="text-danger">{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
