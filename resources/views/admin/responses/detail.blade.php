@extends('layouts.main')

@section('content')
    <div class="main-content">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h1>Detail Response</h1>
                        <form action="/dashboard/responses/{{ $response->id }}" method="POST">
                            @csrf
                            @method('PATCH')
                            <div class="row flex-column">
                                <div class="col-2 mb-3">
                                    <label for="Response Date" class="form-label">Response Date</label>
                                    <input type="date" class="form-control" id="response_date" name="response_date"
                                        value="{{ $response->response_date }}">
                                </div>
                                <div class="col-5 mb-3">
                                    <label for="Response" class="form-label">Response</label>
                                    <input type="text" class="form-control" id="response" name="response"
                                        value="{{ $response->response }}">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
