@extends('app')

@section('content')
    <div class="container">
        <h1>Add Response</h1>
        <form action="/dashboard/responses" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-2 mb-3">
                    <label for="report_date" class="form-label">Report Date</label>
                    <input type="text" class="form-control" id="report_date" name="report_date">
                </div>
                <div class="col-2 mb-3">
                    <label for="report" class="form-label">Report</label>
                    <input type="text" class="form-control" id="report" name="report">
                </div>
                <div class="col-2 mb-3">
                    <label for="Response Date" class="form-label">Response Date</label>
                    <input type="text" class="form-control" id="response_date" name="response_date">
                </div>
                <div class="col-5 mb-3">
                    <label for="Response" class="form-label">Response</label>
                    <input type="text" class="form-control" id="response" name="response">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
    </form>
    @if ($errors->any())
        @foreach ($errors->all as $error)
            <p class="text-danger">{{ $error }}</p>
        @endforeach
    @endif
    </div>
@endsection