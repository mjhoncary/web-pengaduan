@extends('layouts.main')

@section('content')
    <div class="main-content">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h1>Data Responses</h1>
                        <p>{{ $response_list->links() }}</p>
                        <table class="table">
                            <thead>
                                <tr class="border">
                                    <th>No.</th>
                                    <th>Report</th>
                                    <th>Response Date</th>
                                    <th>Response</th>
                                    <th>Responded by</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($response_list as $response)
                                    <tr class="border">
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $response->report->report }}</td>
                                        <td>{{ $response->response_date }}</td>
                                        <td>{{ $response->response }}</td>
                                        <td>{{ $response->user->username }}</td>
                                        <td>
                                            <a href="/dashboard/responses/{{ $response->id }}" class="btn btn-primary"><i class="bi bi-pencil-square"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('sweetalert::alert')
@endsection
