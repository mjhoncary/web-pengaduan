@extends('app')

@section('content')
    <div class="container">
        <h1>Detail User</h1>
        <form action="/dashboard/users/{{ $user->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="{{ $user->username }}" disabled>
                </div>
            <div class="col-3 mb-3">
                <label class="form-label">Level</label>
                <select name="level" id="form-select" disabled>
                    @foreach (['admin', 'staff', 'student'] as $item)
                        <option value="{{ $item }}" {{ $user->level == $item ? 'selected' : '' }}>{{ $item }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-3 mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" class="form-control" name="password" id="password">
            </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
    </form>
    </div>
@endsection