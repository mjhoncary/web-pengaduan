@extends('layouts.main')

@section('content')
    <div class="main-content">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">

                    <div class="card-body">



                        <h1>Data User</h1>
                        <p>{{ $user_list->links() }}</p>
                        <table class="table">
                            <thead>
                                <tr class="border">
                                    <th>No</th>
                                    <th>Username</th>
                                    <th>Level</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($user_list as $user)
                                    <tr class="border">
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $user->username }}</td>
                                        <td>{{ $user->level }}</td>
                                        <td>
                                            <a href="/dashboard/users/{{ $user->id }}" class="btn btn-primary"><i class="fa-solid fa-pen-to-square" style="font-size: 15px"></i></a>
                                            <a href="#" class="btn btn-danger" data-toggle="modal"
                                                data-target="#modal-{{ $user->id }}"><i class="fa-solid fa-trash" style="font-size: 15px"></i></a>
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="modal-{{ $user->id }}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Confirm</h5>
                                                    <button type="button" class="btn-close"
                                                        data-bs-dismiss="modal"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>User ID {{ $user->id }} will be deleted</p>
                                                    <p>Continue?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <form action="/dashboard/users/{{ $user->id }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                        <button type="submit" class="btn btn-secondary"
                                                            data-bs-dismiss>Cancel</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </tbody>
                        </table>
                        <a href="/dashboard/users/create" class="btn btn-success"><i class="fa-solid fa-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('sweetalert::alert')
@endsection
