@extends('layouts.main')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
          <div class="card">
          
            <div class="card-body">
    


        <h1>Generate Reports</h1>
        <form action="/dashboard/generate_report" method="GET">
            <div class="row">
                <label for="start" class="col-1 col-form-label">From</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="start" name="start" value="{{ $start }}">
                </div>
                <label for="end" class="col-1 col-form-label">Until</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="end" name="end" value=  "{{ $end }}">
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-success d-print-none">Search</button>
                </div>
            </div>
        </form>
        <table class="table border">
            <thead>
                <tr class="border">
                    <th>No.</th>
                    <th>Report Date</th>
                    <th>Name</th>
                    <th>Report</th>
                    <th>Photo</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($report_list as $report)
                    <tr class="border-bottom">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $report->report_date }}</td>
                        <td>{{ $report->student->name }}</td>
                        <td>{{ $report->report }}</td>
                        <td> <img src="{{ asset('storage/' . $report->photo) }}" class="mt-2 mb-2" style="width: 200px; height: 100px"></td>
                       
                    </tr>
                @endforeach
            </tbody>
        </table>
            <div class="button button-print d-print-none" onclick="window.print()">
                <div class="button-wrapper">
                  <div class="text">Print</div>
                    <span class="icon">
                      <svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" height="2em" width="2em" role="img" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"><path d="M12 15V3m0 12l-4-4m4 4l4-4M2 17l.621 2.485A2 2 0 0 0 4.561 21h14.878a2 2 0 0 0 1.94-1.515L22 17" stroke-width="2" stroke-linejoin="round" stroke-linecap="round" stroke="currentColor" fill="none"></path></svg>
                    </span>
                  </div>
                </div>
    </div>
</div>
</div>
</div>
</div>
</div>

@endsection