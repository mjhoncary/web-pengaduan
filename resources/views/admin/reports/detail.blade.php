@extends('layouts.main')

@section('content')
    <div class="main-content">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h1>Detail Report</h1>
                        <form action="/dashboard/reports/{{ $report->id }}" method="POST">
                            @csrf
                            @method('PATCH')
                            <div class="row flex-column">
                                <div class="col-2 mb-3">
                                    <label for="report_date" class="form-label">Report Date</label>
                                    <input type="text" class="form-control" id="report_date" name="report_date"
                                        value="{{ $report->report_date }}" disabled>
                                </div>
                                <div class="col-4 mb-3">
                                    <label for="report" class="form-label">Report</label>
                                    <input type="text" class="form-control" id="report" name="report"
                                        value="{{ $report->report }}" disabled>
                                </div>
                                <div class="col-3 mb-3">
                                    <label class="form-label">Status</label>
                                    <select name="status" class="form-control" id="form-select">
                                        @foreach (['sent', 'processed', 'completed'] as $item)
                                            <option value="{{ $item }}"
                                                {{ $report->status == $item ? 'selected' : '' }}>
                                                {{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4 mb-3">
                                    <label for="proof_file" class="form-label">Proof</label>
                                    <input type="file" class="form-control" id="proof_file" name="proof_file"
                                        accept="image/png,image/jpeg" disabled>
                                    <img src="{{ asset('storage/' . $report->photo) }}" style="width: 300px">
                                </div>
                                <div class="col-4 mb-3">
                                    <label for="response" class="form-label">Response</label>
                                    <input type="text" class="form-control" id="response" name="response" value="{{ $report->response ? $report->response->response : '' }}">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </form>
                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <p class="text-danger">{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
