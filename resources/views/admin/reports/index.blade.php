@extends('layouts.main')

@section('content')
    <div class="main-content">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">

                        <h1>All Reports</h1>
                        <p>{{ $report_list->links() }}</p>
                        <table class="table border">
                            <thead>
                                <tr class="border">
                                    <th>No.</th>
                                    <th>Report Date</th>
                                    <th>Name</th>
                                    <th>Report</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($report_list as $report)
                                    <tr class="border-bottom">
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $report->report_date }}</td>
                                        <td>{{ $report->student->name }}</td>
                                        <td>{{ $report->report }}</td>
                                        <td>{{ $report->status }}</td>
                                        <td>
                                            <a href="/dashboard/reports/{{ $report->id }}" class="btn btn-primary"><i
                                                    class="fa-solid fa-pen-to-square" style="font-size: 15px"></i></a>
                                            <a href="#" class="btn btn-danger" data-toggle="modal"
                                                data-target="#modal-{{ $report->id }}"><i class="fa-solid fa-trash"
                                                    style="font-size: 15px"></i></a>
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="modal-{{ $report->id }}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Confirm</h5>
                                                    <button type="button" class="btn-close"
                                                        data-bs-dismiss="modal"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>User ID {{ $report->id }} will be deleted</p>
                                                    <p>Continue?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <form action="/dashboard/reports/{{ $report->id }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                        <button type="submit" class="btn btn-secondary"
                                                            data-bs-dismiss>Cancel</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('sweetalert::alert')
@endsection
