@extends('app')

@section('content')
    <div class="container">
        <h1>Create Report</h1>
        <form action="/dashboard/reports" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row flex-column">
                <div class="col-2 mb-3">
                    <label for="report_date" class="form-label">Report Date</label>
                    <input type="date" class="form-control" id="report_date" name="report_date">
                </div>
                <div class="col-4 mb-3">
                    <label class="form-label">Student ID</label>
                    <select name="student_id" class="form-select">
                        @foreach ($student_list as $student)
                            <option value="{{ $student->id }}">{{ $student->id }} - {{ $student->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-4 mb-3">
                    <label for="Report" class="form-label">Report</label>
                    <input type="text" class="form-control" id="report" name="report">
                </div>
                <div class="col-3 mb-3">
                    <label class="form-label">Status</label>
                    <select name="status" id="form-select">
                        @foreach (['sent', 'proccesed', 'completed'] as $item)
                            <option>"{{ $item }}"</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-3 mb-3">
                    <label for="photo" class="form-label">Photo</label>
                    <input type="file" class="form-control" id="photo" name="photo" accept="image/png,image/jpeg">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
    @include('sweetalert::alert')
@endsection
