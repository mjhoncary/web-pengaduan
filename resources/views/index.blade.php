<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body>
    <form method="POST">
        <h1>Login</h1>
        <div class="form-floating">
            <input type="text" name="username"><label for="username">Username</label>
        </div>
        <div class="form-floating">
            <input type="password" name="password"><label for="password">Password</label>
        </div>
        <button class="w-100 btn btn-lg btn-primary" type="submit" name="login">LOGIN</button>
    </form>
</body>
</html>