<header class="navbar-fixed-top" id="navigation-bar">
  <div class="container-fluid d-flex align-items-center d-print-none">
    <a href="" class="text-decoration-none">
      <h2 class="judul">GPS</h2>
      <h6 class="judul-2">Gracia Peduli Sesama</h6>
    </a>
    <ul class="nav ms-auto">
      <li><a href="/student/reports/create" class="nav-link" style="font-size: 20px">HOME</a></li>
      <li><a href="#about" class="nav-link me-5" style="font-size: 20px">ABOUT</a></li>
    </ul>
    <div class="dropdown">
      <img src="/img/profile.png" class="me-2" alt="" style="width: 40px">
      <a href="" class="name dropdown-toggle text-decoration-none me-5" data-bs-toggle="dropdown">Hello, {{ auth()->user()->username }}</a>
      <ul class="dropdown-menu">
        <li><a href="/student/reports" class="dropdown-item">Report History</a></li>
        <li><form action="/logout" method="post">
          @csrf
          <button type="submit" class="dropdown-item">Logout</button>
        </form>

        </li>
      </ul>
    </div>
  </div>
  <style>
    header {
      position: sticky;
      width: 100%;
      height: 100px;
      top: 0;
      z-index: 9999;
      padding-bottom: 10px;
    }
  </style>
</header>