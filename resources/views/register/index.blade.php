@extends('app2')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-4">

        <main class="form-registration">
            <h1 class="h3 mb-3 fw-normal text-center mt-4">Sign Up</h1>
            <form action="/register" method="post">
                @csrf
              <div class="form-floating">
                <input type="text" name="name" class="form-control @error('name')is-invalid @enderror" id="name" placeholder="Name" required value="{{ old('name') }}">
                <label for="name"><i class="fa-solid fa-circle-user ms-1 me-2" style="opacity: 0.6;font-size: 15px"></i> Name</label>
                @error('name')
                <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                  @enderror
              </div>
              <div class="form-floating">
                <input type="text" name="phone" class="form-control @error('phone')is-invalid @enderror" id="phone" placeholder="Phone" required value="{{ old('phone') }}">
                <label for="phone"><i class="fa-solid fa-phone ms-1 me-2" style="opacity: 0.6;font-size: 13px"></i> Phone</label>
                @error('phone')
                <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                  @enderror
              </div>
              <div class="form-floating">
                <input type="text" name="address" class="form-control @error('address')is-invalid @enderror" id="address" placeholder="Address" required value="{{ old('address') }}">
                <label for="address"><i class="fa-solid fa-location-dot me-2 ms-1" style="opacity: 0.6"></i> Address</label>
                @error('address')
                <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                  @enderror
              </div>
              <div class="form-floating">
                <input type="text" name="grade" class="form-control @error('grade')is-invalid @enderror" id="grade" placeholder="Grade" required value="{{ old('grade') }}">
                <label for="grade"><i class="fa-solid fa-graduation-cap ms-1 me-2" style="opacity: 0.6;font-size: 13px"></i> Grade</label>
                @error('grade')
                <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                  @enderror
              </div>
              <div class="form-floating">
                <input type="text" name="username" class="form-control @error('username')is-invalid @enderror" id="username" placeholder="Username" required value="{{ old('username') }}">
                <label for="username"><i class="fa-solid fa-user me-2 ms-1" style="opacity: 0.6;font-size: 13px"></i> Username</label>
                @error('username')
                <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                  @enderror 
              </div>
              <div class="form-floating">
                <input type="password" name="password" class="form-control @error('password')is-invalid @enderror" id="username" id="floatingPassword" placeholder="Password" required>
                <label for="floatingPassword"><i class="fa-solid fa-eye me-2" style="opacity: 0.6;font-size: 13px"></i> Password</label>
                @error('password')
                <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                  @enderror
              </div>
              <button class="w-100 btn btn-lg btn-primary mt-3" type="submit">SIGN UP</button>
            </form>
            <small class="d-block text-center">Already have an account? <a href="/login">Login</a></small>
          </main>
    </div>
</div>


@endsection