@extends('app2')

@section('content')
    <div class="masthead">
        <div class="container">
            <div class="row">
                <div class="col">
                    <img src="/img/welcome.jpg" class="img-fluid" style="height: auto;" alt="Responsive image">
                </div>
                <div class="col-5">
                    <h1 class="title-home"><b>Misi Kami</b></h1>
                    <h3 class="sub-title-home">Membantu mengurangi kasus perundungan di sekolah GRACIA melalui
                        program-program<br>yang diberikan</h3>
                    <hr>
                    <a type="button" href="/login" class="text-button btn btn-light"><b>Login</b> <i
                            class="bi bi-arrow-right-circle"></i></a>
                </div>
            </div>
        </div>
    @endsection

    