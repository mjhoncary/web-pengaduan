<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
</head>
<body>
    <div class="row">
        <div class="col-md-5">
            <div class="container">
                <div class="login">
                    <div class="content">
                        <img src="/img/img-login.jpg" alt="" style="width: 300px">
                    </div>
                    <div class="loginform">
                        <i class="bi bi-person-circle"></i>
                        <h1>Login</h1>
                        <form action="">
                            <div class="tbox">
                                <i><input type="text" name="" placeholder="Email"></i>
                            </div>
                            <div class="tbox">
                                <i><input type="password" name="" placeholder="Password"></i>
                            </div>
                            <div class="checkbox mb-3 r-me">
                                <label>
                                  <input type="checkbox" value="remember-me"> Remember me
                                </label>
                              </div>
                            <button class="btn">LOGIN</button>
                            <div class="text-down ms-3">
                                <h5 class="register">Don't have a account? <a href="/register" class="register-2" style="color: white;">Sign Up</a></h5>
                            </div>
                        </form>
                            <style>
                                *{
                                    margin: 0px;
                                    padding: 0px;
                                    box-sizing: border-box;
                                }
                                .container {
                                    background: #f2f2f2;
                                    display: flex;
                                    justify-content: center;
                                    align-items: center;
                                    min-height: 100vh;
                                }
                                .login {
                                    width: 600px;
                                    min-height: 400px;
                                    overflow: hidden;
                                    border-radius: 5px;
                                    background: #222;
                                    display: flex;
                                    box-shadow: 15px 5px 30px rgba(0, 0, 0, 0.3)
                                }
                                .content {
                                    flex-basis: 50%;
                                    display: flex;
                                    justify-content: center;
                                    align-items: center;
                                    flex-direction: column;
                                    background: white
                                }
                                .content img {
                                    width: 100%;
                                }
                                .loginform {
                                    flex-basis: 50%;
                                    background: #1F4E62;
                                    font-family: roboto;
                                    padding: 25px;
                                }
                                .loginform h1 {
                                    text-align: center;
                                    margin-top: 25px;
                                    font-size: 2.5em;
                                    margin-bottom: 50px;
                                    color: #fff;
                                }
                                .tbox {
                                    border-radius: 3px;
                                    background: white;
                                    outline: none;
                                    border: 1px solid #fff;
                                    width: 100%;
                                    height: 35px;
                                    margin: 10px 0;
                                }
                                .tbox input {
                                    background: white;
                                    width: 90%;
                                    height: 30px;
                                    outline: #222;
                                    border: 0;
                                    padding-left: 5px;
                                }
                                ::placeholder {
                                    color: #444444;
                                    opacity: 0.6;
                                }
                                .tbox i {
                                    color: gray;
                                    font-size: 1px;
                                    padding: 7px;
        
                                }
                                .btn {
                                    width: 100%;
                                    border: 0;
                                    border-radius: 5px;
                                    background: #1CAF4A;
                                    color: #fff;
                                    padding: 12px;
                                    font-size: 18px;
                                }
                                .btn:hover {
                                    background: #147b2f;
                                    color: #dadada
                                }
                                .text-down {
                                    color: white;
                                    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
                                    font-size: 14px;
                                }
                                .register {
                                    text-align: center;
                                    margin-top: 10px
                                }
                                .register a:hover {
                                    color: burlywood;
                                }
                                .r-me {
                                    color: white;
                                    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
                                    margin-bottom: 10px;
                                    font-size: 13px
                                    
                                }
                            </style>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>