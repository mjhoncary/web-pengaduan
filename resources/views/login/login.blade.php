<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/fontawesome.min.css">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <title>GPS</title>
</head>
<style>
  .right {
    background-color: #1F4E62; 
  }
</style>
<body>
  <div class="container p-5">
    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show mt-5" role="alert">
            {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
  
    @if (session()->has('loginError'))
        <div class="alert alert-danger alert-dismissible fade show mt-5" role="alert">
            {{ session('loginError') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="row d-flex justify-content-center mt-2 pt-5">
      <div class="left col-md-3 border">
        <img src="/img/img-login.jpg" class="mt-3" style="width: 25rem" alt="">
      </div>
        <div class="right col-md-5">


            <main class="form-signin">
                <i class="bi bi-person-circle d-block text-center mb-1 mt-4" style="font-size: 40px;opacity: 0.86"></i>
                <h1 class="h3 mb-3 fw-normal text-center" style="opacity: 0.8"><b>Login</b></h1>
                <form class="form-signin" action="/login" method="post">
                    @csrf
                    <div class="form-floating">
                        <input type="text" name="username"
                            class="form-control @error('username') is-invalid @enderror" id="username"
                            placeholder="Username" required value="{{ old('username') }}">
                        <label for="username"><i class="fa-solid fa-user me-2 ms-1"
                                style="opacity: 0.6;font-size: 13px"></i style> Username</label>
                        @error('username')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-floating">
                        <input type="password" name="password" class="form-control" id="password"
                            placeholder="Password" required>
                        <label for="password"><i class="fa-solid fa-eye me-2" style="opacity: 0.6;font-size: 13px"></i>
                            Password</label>
                    </div>
                    <button class="w-100 btn btn-lg btn-primary" type="submit">LOGIN</button>
                </form>
                <small class="d-block text-center">Don't have an account? <a href="/register">Sign Up</a></small>
            </main>
        </div>
    </div>
  </div>
    <script src="{{ mix('js/app.js') }}"></script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>
