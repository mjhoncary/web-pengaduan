<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="header finisher-header" style="width: 100%; height: 40rem;">
        <h1 class="m-3">Hi</h1>
        <h1 class="m-3">Hi</h1>
        <h1 class="m-3">Hi</h1>
      </div>

    <script type="text/javascript">
        new FinisherHeader({
          "count": 10,
          "size": {
            "min": 2,
            "max": 40,
            "pulse": 0
          },
          "speed": {
            "x": {
              "min": 0,
              "max": 0.8
            },
            "y": {
              "min": 0,
              "max": 0.2
            }
          },
          "colors": {
            "background": "#1f4e62",
            "particles": [
              "#ff926b",
              "#87ddfe",
              "#acaaff",
              "#21ff1b",
              "#f9a5fe"
            ]
          },
          "blending": "screen",
          "opacity": {
            "center": 1,
            "edge": 1
          },
          "skew": -1,
          "shapes": [
            "c",
            "s",
            "t"
          ]
        });
        </script>
</body>
</html>