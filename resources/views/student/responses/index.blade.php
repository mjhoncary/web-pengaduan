@extends('app')

@section('content')

<div class="container">
    <h1></h1>
    <p>{{ $response_list->links() }}</p>
    <table class="table">
        <thead>
            <tr>
                <th>No.</th>
                <th>Report</th>
                <th>Response</th>
                <th>Response Date</th>
                <th>Operator</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($response_list as $response)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $response->report->report }}</td>
                    <td>{{ $response->response }}</td>
                    <td>{{ $response->response_date }}</td>
                    <td>{{ $response->user->username }}</td>
                    <td>
                        <a href="" class="btn btn-primary">Details</a>
                        <a href="" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <a href="" class="btn btn-success">Tambah</a>
</div>
    
@endsection