@extends('app')

@section('content')

    <div class="bg-form p-5 text-center" id="home">
        <div class="row report-box  border m-auto justify-content-center" style="width: 50rem; background-color: #D2CEFF;">
            <h1 class="title-report">Create Report</h1>
            <form action="/student/reports" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row flex-column bg-report">
                    <div class="col-3 m-auto mb-3 mt-2 inputbox">
                        <input required="required" type="date" name="report_date" id="report_date" placeholder="Date">
                        <span></span>
                        <i></i>
                    </div>
                    <div class="col-3 m-auto mb-3 inputbox">
                        <input required="required" type="text" name="name" id="name" placeholder="Name">
                        <i></i>
                    </div>
                    <textarea class="m-auto mb-4" name="report" id="report" style="width: 40rem; border: none" required="required"
                        cols="50" rows="16" placeholder="Report"></textarea>
                    <div class="m-auto">
                        <label for="file-input" class="drop-container mb-4">
                            <span class="drop-title">Proof photo</span>

                            <input type="file" class="form-control" accept="image/png,image/jpeg" name="photo"
                                id="photo">
                        </label>
                    </div>
                </div>
                <button class="btn-report mb-4" type="submit">Report!</button>
            </form>
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <p class="text-danger">{{ $error }}</p>
                @endforeach
            @endif
        </div>
    </div>

    {{-- about --}}
    <div class="container" id="about">
        <h1 class="title-about pb-5 pt-5">About Bullying <i class="bi bi-arrow-down"></i></h1>
        <div class="grid">
            <div class="grid-child">
                <h2 class="about-title">What Is Bullying?</h2>
                <p class="about-text">Bullying is a behavior that involves repeated aggressive actions or negative behavior
                    towards a person who has difficulty defending themselves. It can take various forms, such as physical,
                    verbal, or emotional abuse, and can occur in different settings, including schools, workplaces, or
                    online. The intention of bullying is to harm, intimidate, or dominate others, which can result in
                    lasting psychological and physical harm. Bullying can have severe consequences, including depression,
                    anxiety, and even suicide. It is a serious issue that requires immediate attention and intervention to
                    prevent its occurrence and protect the victims from its harmful effects.</p>
            </div>
            <div class="grid-child ms-auto">
                <img class="img-about" src="/img/about.svg" alt="" width="480px">
            </div>
        </div>
    </div>

    {{-- Card --}}

    <div class="container">
        <div class="mid-home">
            <h1 class="title-mid pb-5">4 Types Of Bullying</h1>
            <div class="grid-container">
                <div class="grid-child">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <p class="title">Relational<br>Bullying</p>
                            </div>
                            <div class="flip-card-back">
                                <img src="/img/relational.svg" class="m-auto" alt="" width="125px">
                                <p class="mb-3 me-3 ms-3">Relational bullying involves harming someone's social connections
                                    or reputation, often through rumor-spreading, or manipulation. It can cause emotional
                                    distress and long-term damage to the victim's self-esteem.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-child">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <p class="title">Physical<br>Bullying</p>
                            </div>
                            <div class="flip-card-back">
                                <img src="/img/physical.png" class="m-auto" alt="" width="125px">
                                <p class="mb-3 me-3 ms-3">Physical bullying involves the use of physical force to harm
                                    someone. This type of bullying can take hitting, pushing, kicking, tripping, spitting,
                                    or damaging someone's. Physical bullying often leaves visible marks or injuries. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-child">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <p class="title">Verbal<br>Bullying</p>
                            </div>
                            <div class="flip-card-back">
                                <img src="/img/verbal.png" class="m-auto" alt="" width="125px">
                                <p class="mb-3 me-3 ms-3">Verbal bullying is the use of words or language to harm,
                                    intimidate, or manipulate someone. This type of bullying can take many forms, including
                                    name-calling, teasing, insults, threats, and spreading rumors or lies.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-child">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <p class="title">Cyber<br>Bullying</p>
                            </div>
                            <div class="flip-card-back">
                                <img src="/img/cyber.png" class="m-auto" alt="" width="125px">
                                <p class="mb-3 me-3 ms-3">Cyber bullying is a form of bullying that takes place online or
                                    through other digital things. This type of bullying can include sending hurtful
                                    messages, posting humiliating content, spreading rumors, and impersonating someone else.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Last --}}

    <div class="container">
        <h1 class="title-last pb-4 pt-5">If You Got Bullied</h1>
        <div class="last">
            <div class="grid-child">
                <img src="/img/social.svg" width="480px" alt="">
            </div>
            <div class="grid-child m-auto">
                <li class="list">Talk to someone you trust: Whether it's a parent, teacher, counselor, or friend, it's
                    important to reach out to someone you trust and tell them what's going on</li>
                <li class="list">Keep a record of the incidents of bullying, including dates, times, locations, and what
                    happened.</li>
                <li class="list">Report the bullying: If the bullying is happening at school, report it to a teacher,
                    counselor, or principal. If it's happening outside of school, report it to the police.</li>
                <li class="list">Take care of yourself: Bullying can be emotionally and physically exhausting. Make sure
                    to take care of yourself by getting enough sleep, eating healthy, and engaging in activities that you
                    enjoy</li>
            </div>
        </div>
    </div>
    @include('sweetalert::alert')
@endsection
