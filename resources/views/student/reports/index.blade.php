@extends('app3')

@section('content')

    <div class="container">
        <h1 class="mt-4">Report History</h1>
        <table class="table border mt-4">
            <thead>
                <tr style="background: #d7d7d7">
                    <th class="border">No.</th>
                    <th class="border">Report Date</th>
                    <th class="border">Name</th>
                    <th class="border">Report</th>
                    <th class="border">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($report_list as $report)
                    <tr>
                        <td class="border">{{ $loop->iteration }}</td>
                        <td class="border">{{ $report->report_date }}</td>
                        <td class="border">{{ $report->student->name }}</td>
                        <td class="border">{{ $report->report }}</td>
                        <td>
                            <a href="/student/reports/{{ $report->id }}" class="btn btn-primary">Detail</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/student/reports/create" class="btn btn-success">Report</a>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>

@endsection
