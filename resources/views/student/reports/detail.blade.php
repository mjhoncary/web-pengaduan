@extends('app')

@section('content')

<div class="container">
    <h1>Detail Report</h1>
    <form action="/student/reports/{{ $report->id }}" method="POST">
        @csrf
        @method('PATCH')
        <div class="row flex-column">
            <div class="col-5 mb-3">
                <label for="report_datedate" class="form-label">Date</label>
                <input type="date" class="form-control" name="report_date" id="report_date" value="{{ $report->report_date }}" disabled>
            </div>
            <div class="col-5 mb-3">
                <label for="name" class="form-label">Name</label>
                <input type="text" class="form-control" name="name" id="name" value="{{ $report->student->name }}" disabled>
            </div>
            <div class="col-5 mb-3">
                <label for="text" class="form-label">Report</label>
                <input type="text" class="form-control" name="report" id="report" value="{{ $report->report }}" disabled>
            </div>
            <div class="col-3 mb-3">
                <label class="form-label">Status</label>
                <select name="status" id="form-select" disabled>
                    @foreach (['sent', 'processed', 'completed'] as $item)
                        <option value="{{ $item }}"
                            {{ $report->status == $item ? 'selected' : '' }}>
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-4 mb-3">
                <label for="photo" class="form-label">Photo</label>
                <input type="file" name="form-control" id="photo" accept="image/png,image/jpeg" disabled>
                <img src="{{ asset('storage/' . $report->photo) }}" style="width: 300px" alt="">
            </div>
            @if($report->response)
            <div class="col-2 mb-3">
                <label for="Response Date" class="form-label">Response Date</label>
                <input type="text" class="form-control" id="response_date" name="response_date" value="{{ $report->response->response_date }}" readonly>
            </div>
            <div class="col-7 mb-3">
                <label for="Response" class="form-label">Response</label>
                <input type="text" class="form-control" id="response" name="response" value="{{ $report->response->response }}" readonly>
            </div>
            @endif
        </div>
        <a href="/student/reports" class="btn btn-success">Back</a>
    </form>
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <p class="text-danger">{{ $error }}</p>
        @endforeach
        
    @endif
</div>
    
@endsection