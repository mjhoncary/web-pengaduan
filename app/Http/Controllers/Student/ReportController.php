<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Report;
use App\Models\Student;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $reports = $user->student->reports;
        return view('student.reports.index', [
            'report_list' => $reports,
            "title" => "Home"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all();
        return view('student.reports.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'report_date' => 'required',
            'report' => 'required',
            'photo' => 'required'
        ]);
    
        $path = $request->photo->store('public/images');
        $data['photo'] = str_replace('public/', '', $path);

        $data['student_id'] = Auth::user()->student->id;

        Report::create($data);


        return redirect('/student/reports/create')->with('success', 'Report Submitted!');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = Report::find($id);

        $user = Auth::user();
        if ($report->student->user->id != $user->id) {
            return redirect('/student/reports');
        }

        return view('student.reports.detail', ['report' => $report]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Report::destroy($id);
            return redirect('/student/reports');
        } catch (QueryException $exc) {
            return redirect('/student/reports')
            ->withErrors([
                'msg' => 'Report ' . $id . ' cannot be deleted because related with other entity'
            ]);
        }
    }
}
