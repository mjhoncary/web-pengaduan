<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;

class RegisterController extends Controller
{
    public function index()
    {
        return view('register.signup', [
            'title' => 'Sign Up'
        ]);
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => ['required', 'max:255'],
            'phone' => ['required', 'max:14'],
            'address' => ['required', 'max:100'],
            'grade' => ['required', 'max:5'],
            'username' => ['required', 'unique:users'],
            'password' => ['required'] 
        ]);
                    
        $user = User::create([
            'username' => $validatedData['username'],
            'password' => Hash::make($validatedData['username']),
        ]);

        Student::create([
            'name' => $validatedData['name'],
            'grade' => $validatedData['grade'],
            'phone' => $validatedData['phone'],
            'address' => $validatedData['address'],
            'user_id' => $user->id
        ]);

        User::create($validatedData);
        $request->session()->flash('success', 'Sign Up Successfull! Please Login');
        return redirect('/login');
    }
}
