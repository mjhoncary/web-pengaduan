<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Report;
use App\Models\Response;
use App\Models\Student;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::paginate(5);
        return view('admin.reports.index', ['report_list' => $reports]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all();
        return view('admin.reports.create', ['student_list' => $students]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'report_date' => 'required',
            'student_id' => 'required',
            'report' => 'required',
            'status' => 'required',
            'photo' => 'required|file',
        ]);

        $path = $request->photo->store('public/images');
        $data['photo'] = str_replace('public/', '', $path);

        Report::create($data);

        return redirect('/dashboard/reports');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = Report::find($id);
        return view('admin.reports.detail', ['report' => $report]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'status' => 'required',
            'response' => 'required'
        ]);

        $report = Report::where('id', $id);
        $report->update(['status' => $data['status']]);

        $response = Response::where('report_id', $report->first()->id);
        if ($response->first()) {
            $response->update(['response' => $data['response']]);
        } else {
            Response::create([
                'report_id' => $report->first()->id,
                'response_date' => $report->first()->updated_at,
                'response' => $data['response'],
                'user_id' => Auth::user()->id
            ]);
        }
        
        return redirect('/dashboard/reports')->with('success', 'Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Report::destroy($id);
            return redirect('/dashboard/reports')->with('success', 'Report Deleted!');
        } catch (QueryException $exc) {
            return redirect('/dashboard/reports')
            ->withErrors([
                'msg' => 'Report ' . $id . ' cannot be deleted because related with other entity'
            ]);
        }
    }
}
