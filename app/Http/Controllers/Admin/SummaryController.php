<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Report   ;

class SummaryController extends Controller
{
    public function printReport(Request $request)
    {
        $data = $request->validate([
            'start' => 'filled',
            'end' => 'filled'
        ]);

        if(array_key_exists('start', $data) && array_key_exists('end', $data)) {
            $reports = Report::where('report_date', '>=', $data['start'])
                ->where('report_date', '<=', $data['end'])->get();
                $start = $data['start'];
                $end = $data['end'];
        } else {
            $reports = Report::all();
            $start = '';
            $end = '';
        }

        return view('admin.reports.summary', [
            'report_list' => $reports, 'start' => $start, 'end' => $end
        ]);
    }
}
