<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    public function show()
    {
        return view('login.login2', [
            'title' => 'Login'
        ]);
    }

    
    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'username' => ['required', 'min:5', 'max:20'],
            'password' => ['required', 'min:5']
        ]);


        if(Auth::attempt($credentials)) {
            $request->session()->regenerate();
          
            $user = Auth::user();
            if ($user->level == 'admin') {
                return redirect('/dashboard');
            } else if ($user->level == 'staff') {
                return redirect('/dashboard');
            } else if ($user->level == 'student') {
                return redirect('/student/reports/create');
            }
        }
        
        return back()->with('loginError', 'Login Failed!');
    }
    public function logout(Request $request)
    {
        Auth::logout();

        request()->session()->invalidate();
        request()->session()->regenerateToken();
        return redirect('/');
    }
}
