<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
      protected $fillable = [
        'report_date',
        'student_id',
        'report',
        'photo',
        'status',
        'created_at',
        'updated_at',
    ];
    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }
    public function response()
    {
        return $this->hasOne(Response::class, 'report_id');
    }
}
