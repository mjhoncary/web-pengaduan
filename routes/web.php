<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Admin\StudentController as AdminStudentController;
use App\Http\Controllers\Admin\ReportController as AdminReportController;
use App\Http\Controllers\Admin\ResponseController as AdminResponseController;
use App\Http\Controllers\Student\ReportController as StudentReportController;
use App\Http\Controllers\Student\ResponseController as StudentResponseController;
use App\Http\Controllers\Admin\SummaryController as AdminSummaryController;

use App\Http\Controllers\AdminController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function() {

Route::get('/dashboard', [AdminController::class, 'index']);
    
Route::resource('/dashboard/users', AdminUserController::class);
Route::resource('/dashboard/students', AdminStudentController::class);
Route::resource('/dashboard/reports', AdminReportController::class);
Route::resource('/dashboard/responses', AdminResponseController::class);
Route::get('dashboard/generate_report', [AdminSummaryController::class, 'printReport']);

Route::resource('/student/reports', StudentReportController::class);
Route::resource('/student/responses', StudentResponseController::class);
Route::get('/home', [HomeController::class, 'index']);
});

Route::get('/login', [LoginController::class, 'show'])->name('welcome')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);
Route::get('/register', [RegisterController::class, 'index'])->middleware('guest');
Route::post('/register', [RegisterController::class, 'store']);